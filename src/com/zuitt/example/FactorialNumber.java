package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {

        public static void main(String[] args) {
//            Scanner input = new Scanner(System.in);
//            int num = 0, i = 1;
//            long factorial = 1;
//            System.out.println("Please enter a number:");
//            num = input.nextInt();
//            while(i <= num)
//            {
//                factorial *= i;
//                i++;
//            }
//
//            System.out.printf("Factorial of %d = %d", num, factorial);
            System.out.println("Please enter a number:");
            Scanner input1 = new Scanner(System.in);



            try {
                int num1 = input1.nextInt();
                int forloopfactorial = 1;
                int sample = 1;

                if(num1 >= 0) {
                        if(num1 == 0){
                            System.out.println("Factorial of 0 is 1.");
                        }else {
                            for (sample = 1; sample <= num1; sample++) {
                                forloopfactorial *= sample;
                            }
                            System.out.println("The factorial of " + num1 + " is " + forloopfactorial);
                        }
                    } else{
                        System.out.println("Please enter a positive number.");
                }
            }
            catch (Exception err){
                System.out.println("Please enter a valid number.");

                err.printStackTrace();
            }
        }
    }

